Winter Scene
Seth Hendrick

Instructions:
Use the left and right arrows to control the wind.  The wind will blow all the snowflakes in that direction.  Letting go of the arrow keys stops the wind.

Entites:
The entity that does not move with user interaction is the smoke that comes out the chimney.

The entities that are controlled by user are all the snowflakes, as they are blown by the wind the user controls.
