#include <random>

#include "Smoke.h"
#include "Shape.h"

std::uniform_int_distribution<unsigned int> Smoke::nextDirection(0, 1);

Smoke::Smoke(Shape *s) :
	Entity(s, 0.0f, 0.0f, 0.01f, 0.01f, 3.0f)
{
	reset();
}

Smoke::~Smoke(void)
{
}

void Smoke::update() {
	//Increase the smoke size
	m_scale.x += 0.006f * Entity::deltaTimeScaled;
	m_scale.y += 0.003f * Entity::deltaTimeScaled;

	unsigned int direction = nextDirection(gen);

	//Go left
	if (direction == 0) {
		applyForce(glm::vec3(-0.01f, 0.01f, 0.0f));
	}
	//go right
	else {
		applyForce(glm::vec3(0.01f, 0.01f, 0.0f));
	}

	Entity::update();

	//Reset position if it goes too high
	if ((m_position.y - m_scale.y) > 1.0f) {
		reset();
	}
}

void Smoke::reset() {
	m_scale.x = 0.003f;
	m_scale.y = 0.001f;

	m_position.x = 0.80f;
	m_position.y = 0.25f;

	m_velocity = glm::vec3(0.0f, 0.0f, 0.0f);
	m_acceleration = glm::vec3(0.0f, 0.0f, 0.0f);
}
