#pragma once

#include "Entity.h"
#include "Shape.h"

class SnowmanArm : public Entity {
	public:
		SnowmanArm(Shape *s);
		~SnowmanArm(void);

		void update() override;

	private:
		float m_rotation;

};
