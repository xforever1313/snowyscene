#include <GL/glew.h>
#include <GL/freeglut.h>

#include <cmath>
#include <iostream>
#include <vector>

#include "Entity.h"
#include "ShaderHelpers.h"
#include "Shape.h"
#include "Smoke.h"
#include "Snowflake.h"
#include "SnowmanArm.h"

const int width = 800;
const int height = 600;

//Shape templates
Shape *triangle = nullptr;
Shape *square = nullptr;
Shape *circle = nullptr;

//Entites
std::vector <Snowflake*> snowflakes;
Smoke *smoke = nullptr;

GLuint program = 0;

enum WindDirection {
	LEFT,
	NONE,
	RIGHT
};

WindDirection windDirection = NONE;

std::vector<GLfloat> getCircleVerticies() {
	const unsigned int delta = 100;
	std::vector<GLfloat> verticies;
	float pi = 3.141592f;
	for (float i = 0.0f; i < (2.0f * pi); i += (2.0f * pi / delta)) {
		verticies.push_back(std::cos(i));
		verticies.push_back(std::sin(i));
	}

	return verticies;
}

///Returns true on success
bool init() {
	bool success = false;
	program = loadShaderProgram("shaders/vertexShader.glsl", "shaders/fragmentShader.glsl");
	if (program != 0) {

		const GLfloat triangleVerticies[] = {   //X    Y
												-1,  -1, 
												 0,   1, 
                                                 1,  -1};

		const GLfloat squareVerticies [] = {  // X     Y
											   -1.0f, -1.0f,
											   -1.0f,  1.0f, 
												1.0f,  1.0f, 
												1.0f, -1.0f };

		std::vector<GLfloat> circleVerticies = getCircleVerticies();

		glUseProgram(program);
		triangle = new Shape(triangleVerticies, 3, program);
		square = new Shape(squareVerticies, 4, program);
		circle = new Shape(circleVerticies.data(), circleVerticies.size() / 2, program);
		smoke = new Smoke(circle);

		for (unsigned int i = 0; i < 250; ++i) {
			snowflakes.push_back(new Snowflake(circle));
		}

		success = true;
	}
	else {
		std::cerr << "There was a problem creating the shader program :(" << std::endl;
	}

	glClearColor(0.0f, 0.1f, 0.25f, 1.0f);

	return success;
}

size_t activeSnowflakeIndex = 0;
unsigned int timeSinceLastSnowflake = 0;

void update() {
	Entity::updateDeltaTime();

	//Stagger the snowflakes so they don't all fall at once.
	if (activeSnowflakeIndex < snowflakes.size()){

		//Make a snowflake fall every 50ms
		timeSinceLastSnowflake += Entity::getDeltaTime();
		if (timeSinceLastSnowflake >= 50) {
			timeSinceLastSnowflake = 0;
			++activeSnowflakeIndex;
		}
	}

	//Apply wind direction if needed
	if (windDirection == LEFT) {
		for (size_t i = 0; i < activeSnowflakeIndex; ++i) {
			snowflakes[i]->applyForce(glm::vec3(-0.05f, 0.0f, 0.0f));
		}
	}

	else if (windDirection == RIGHT) {
		for (size_t i = 0; i < activeSnowflakeIndex; ++i) {
			snowflakes[i]->applyForce(glm::vec3(0.05f, 0.0f, 0.0f));
		}
	}

	//Update all the snowflakes
	for (size_t i = 0; i < activeSnowflakeIndex; ++i) {
		snowflakes[i]->update();
	}

	smoke->update();

	glutPostRedisplay();
}

void drawStaticShapes() {
	//Moon
	setShaderColor(program, "colorTint", 0.9f, 0.9f, 1.0f);
	circle->draw(-1.0f, 1.0f, 0.25f, 0.25f);

	//Ground
	setShaderColor(program, "colorTint", 0.7f, 0.7f, 0.7f);
	square->draw(0.0f, -2.7f, 2.0f, 2.0f);

	//House bottom
	setShaderColor(program, "colorTint", 0.6f, 0.0f, 0.0f);
	square->draw(0.6f, -0.43f, 0.3f, 0.3f);

	//Chimney
	setShaderColor(program, "colorTint", 0.41f, 0.0f, 0.0f);
	square->draw(0.8f, 0.1f, 0.1f, 0.2f);

	//Roof
	setShaderColor(program, "colorTint", 0.51f, 0.25f, 0.0f);
	triangle->draw(0.6f, 0.0f, 0.4f, 0.2f);

	//Door
	setShaderColor(program, "colorTint", 0.51f, 0.25f, 0.0f);
	square->draw(0.6f, -0.576f, 0.1f, 0.15f);

	//Door knob
	setShaderColor(program, "colorTint", 0.0f, 0.0f, 0.0f);
	circle->draw(0.65f, -0.576f, 0.03f, 0.03f);

	//Set snowman arm color.
	setShaderColor(program, "colorTint", 0.4f, 0.2f, 0.0f);

	//Left snowman arm
	square->draw(-0.54f, -0.25f, 0.1f, 0.01f);
	square->draw(-0.64f, -0.2f, -0.01f, 0.06f);

	//Right snowman arm
	square->draw(-0.255f, -0.25f, 0.1f, 0.01f);

	//Set snowman color
	setShaderColor(program, "colorTint", 0.65f, 0.65f, 0.65f);

	//Snowman Base
	circle->draw(-0.4f, -0.55f, 0.2f, 0.2f);

	//Snowman Middle
	circle->draw(-0.4f, -0.25f, 0.15f, 0.15f);

	//Snowman Top
	circle->draw(-0.4f, -0.01f, 0.1f, 0.1f);

	//Set snowman feature color
	setShaderColor(program, "colorTint", 0.0f, 0.0f, 0.0f);

	//Top button
	circle->draw(-0.4f, -0.166667f, 0.01f, 0.01f);

	//Middle button
	circle->draw(-0.4f, -0.233333f, 0.01f, 0.01f);

	//bottom button
	circle->draw(-0.4f, -0.29667f, 0.01f, 0.01f);

	//Left eye
	circle->draw(-0.425f, 0.02666f, 0.01f, 0.01f);

	//Right eye
	circle->draw(-0.375f, 0.02666f, 0.01f, 0.01f);

	//Mouth pieces
	circle->draw(-0.4f, -0.07f, 0.01f, 0.01f);
	circle->draw(-0.375f, -0.055f, 0.01f, 0.01f);
	circle->draw(-0.425f, -0.055f, 0.01f, 0.01f);
	circle->draw(-0.35f, -0.03f, 0.01f, 0.01f);
	circle->draw(-0.45f, -0.03f, 0.01f, 0.01f);
}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT);
	
	//Draw smoke
	setShaderColor(program, "colorTint", 0.2f, 0.2f, 0.2f);
	smoke->draw();

	drawStaticShapes();

	//Set snow color
	setShaderColor(program, "colorTint", 0.7f, 0.7f, 0.7f);

	//Draw snowflakes
	for (size_t i = 0; i < snowflakes.size(); ++i) {
		snowflakes[i]->draw();
	}

	glFlush();
}

//Only used for figuring out where points are on the screen
void mouseEvent(int button, int state, int x, int y) {
	std::cout << (static_cast<float>(x) / width) * 2.0f - 1.0f << " " << -((static_cast<float>(y) / height) * 2.0f - 1.0f) << std::endl;
}

void keyboardDown(int key, int x, int y) {
	if (key == GLUT_KEY_LEFT) {
		windDirection = LEFT;
	}
	else if (key == GLUT_KEY_RIGHT) {
		windDirection = RIGHT;
	}
}

void keyboardUp(int key, int x, int y) {
	if (key == GLUT_KEY_LEFT) {
		windDirection = NONE;
	}
	else if (key == GLUT_KEY_RIGHT) {
		windDirection = NONE;
	}
}

int main (int argc, char **argv) {
	int returnCode = 0;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA);
	glutInitWindowSize(width, height);
	glutInitContextVersion(3, 1);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutCreateWindow("Seth's Awesome Scene!");

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		return -1;
	}

	glutIdleFunc(update);
	glutDisplayFunc(draw);
	glutSpecialFunc(keyboardDown); //special func for special keys (e.g. arrow)
	glutSpecialUpFunc(keyboardUp);
	//glutMouseFunc(mouseEvent); //Commented out since this is only needed during development.

	bool initialized = init();

	//Do not want to call the main loop when initlization fails,
	//since the shapes will be set to null, and the program will seg fault when calling a nullptr
	if (initialized) {
		glutMainLoop();
	}
	else {
	    //Fail gracefully
		std::cerr << "Something went wrong during initialization.  Terminating." << std::endl;
		returnCode = 1;
	}

	for (size_t i = 0; i < snowflakes.size(); ++i) {
		delete snowflakes[i];
	}

	delete smoke;

	delete triangle;
	delete square;
	delete circle;

	return returnCode;
}
