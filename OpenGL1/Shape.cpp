#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "ShaderHelpers.h"
#include "Shape.h"

Shape::Shape(const GLfloat *verticies, size_t numberOfVerticies, GLuint shaderProgram) :
	m_vbo(0),
	m_vao(0),
	m_numberVerticies(numberOfVerticies),
	m_shaderProgramIndex(shaderProgram)
{
	//Create and bind vao
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	//Create and bind vertex buffer
	glGenBuffers(1, &m_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	GLuint numValues = numberOfVerticies * 2; //Get total number of values in verticies array.
	glBufferData(GL_ARRAY_BUFFER, numValues * sizeof(GL_FLOAT), verticies, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
}

Shape::~Shape(void) {
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
}

void Shape::draw(const glm::vec3 &position, const glm::vec3 &scale) const {
	draw(position.x, position.y, scale.x, scale.y);
}

void Shape::draw(GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) const {
	glm::mat4 matrix = glm::translate(glm::vec3(x, y, 0.0f)) * glm::scale(glm::vec3(xScale, yScale, 1.0f));
	setShaderMatrix(m_shaderProgramIndex, "worldMatrix", matrix);

	//Draw the shape
	glBindVertexArray(m_vao);
	glDrawArrays(GL_TRIANGLE_FAN, 0, m_numberVerticies);
}
