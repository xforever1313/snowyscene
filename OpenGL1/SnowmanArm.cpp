#include "Entity.h"
#include "Shape.h"
#include "SnowmanArm.h"


SnowmanArm::SnowmanArm(Shape *s) :
	Entity(s, -0.64f, -0.2f, -0.01f, 0.06f, 1.0f),
	m_rotation(0.0f)
{
}


SnowmanArm::~SnowmanArm(void)
{
}


void SnowmanArm::update() {

}