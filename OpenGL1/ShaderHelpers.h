#pragma once

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

char *loadTextFile(const char *file);

GLuint loadShader(const char* file, GLenum shaderType);

GLuint loadShaderProgram(const char *vertexFile, const char *fragmentFile);

void setShaderColor(GLuint shaderProgramIndex, const char *variableName, float r, float g, float b);

void setShaderVec2(GLuint shaderProgramIndex, const char *variableName, const glm::vec2 &vecToSend);

void setShaderMatrix(GLuint shaderProgramIndex, const char *variableName, const glm::mat4 &matrixToSend);
