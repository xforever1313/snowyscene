#pragma once

#include <random>

#include "Entity.h"
#include "Shape.h"

class Smoke : public Entity {
	public:
		Smoke(Shape *s);
		~Smoke(void);

		void update() override;

	private:
		//Used for random numbers
		static std::uniform_int_distribution<unsigned int> nextDirection;

		void reset();
};

