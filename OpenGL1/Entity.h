#pragma once

#include <glm/glm.hpp>
#include <random>

#include "Shape.h"

class Entity {
	public:
		Entity(Shape *shape, float initialX, float initialY, float xScale, float yScale, float mass);
		virtual ~Entity();

		virtual void update();
		void draw();

		void applyForce(const glm::vec3 &force);

		static void updateDeltaTime();

		static unsigned int getDeltaTime();

	protected:
		static unsigned int previousTime;
		static unsigned int deltaTime;
		static float deltaTimeScaled;

		//Used to generate random numbers
		static std::random_device rd;
		static std::mt19937 gen;

		Shape *m_shape;
		glm::vec3 m_position;
		glm::vec3 m_velocity;
		glm::vec3 m_acceleration;
		glm::vec3 m_scale;

		float m_mass;
};
