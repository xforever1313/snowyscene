#version 330

uniform vec4 colorTint; //Color tint

void main() {
	gl_FragColor = colorTint;
}
