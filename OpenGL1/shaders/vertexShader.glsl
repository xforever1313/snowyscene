#version 330

uniform mat4 worldMatrix;

layout (location = 0) in vec2 position;

void main() {
	gl_Position = worldMatrix * vec4(position, 0, 1);
}
