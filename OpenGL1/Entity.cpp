#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <random>

#include "Entity.h"
#include "Shape.h"

unsigned int Entity::previousTime = 0;
unsigned int Entity::deltaTime = 0;
float Entity::deltaTimeScaled = 0.0f;

//Used to generate random numbers.  Used by child classes.
std::random_device Entity::rd;
std::mt19937 Entity::gen(rd());

Entity::Entity(Shape *shape, float initialX, float initialY, float xScale, float yScale, float mass) :
	m_shape(shape),
	m_position(glm::vec3(initialX, initialY, 0)),
	m_velocity(glm::vec3(0, 0, 0)),
	m_acceleration(glm::vec3(0, 0, 0)),
	m_scale(glm::vec3(xScale, yScale, 0)),
	m_mass(mass)
{
}

Entity::~Entity() {
}

void Entity::update() {
	m_velocity += (m_acceleration * Entity::deltaTimeScaled);
	m_position += (m_velocity * Entity::deltaTimeScaled);
	m_acceleration = glm::vec3(0, 0, 0);
}

void Entity::draw() {
	m_shape->draw(m_position, m_scale);
}

void Entity::applyForce(const glm::vec3 &force) {
	m_acceleration += (force / m_mass);
}

void Entity::updateDeltaTime() {
	unsigned int currentTime = glutGet(GLUT_ELAPSED_TIME);
	Entity::deltaTime = currentTime - Entity::previousTime;
	Entity::deltaTimeScaled = Entity::deltaTime * 0.01f;
	Entity::previousTime = currentTime;
}

unsigned int Entity::getDeltaTime() {
	return Entity::deltaTime;
}