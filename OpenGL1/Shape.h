#pragma once

#include <GL/glew.h>
#include <GL/freeglut.h>

#include <glm/glm.hpp>

class Shape {
	public:
		Shape(const GLfloat *verticies, size_t numberOfVerticies, GLuint shaderProgram);
		~Shape(void);

		void draw(const glm::vec3 &position, const glm::vec3 &scale) const;

		void draw(GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) const;

	private:
		GLuint m_vbo;
		GLuint m_vao;
		size_t m_numberVerticies;
		GLuint m_shaderProgramIndex;
};

