#include <fstream>
#include <iostream>

#include "ShaderHelpers.h"

char *loadTextFile(const char *file) {
	char *fileContents = 0;
	std::ifstream inFile = std::ifstream(file, std::ios::binary);
	if (inFile.is_open()) {
		//Get length of file
		inFile.seekg(0, std::ios::end);
		size_t length = static_cast<size_t>(inFile.tellg());
		inFile.seekg(0, std::ios::beg);

		//Read the file
		fileContents = new char[length + 1];
		inFile.read(fileContents, length);
		fileContents[length] = '\0';
		inFile.close();
	}

	return fileContents;
}

GLuint loadShader(const char* file, GLenum shaderType) {
	GLuint ret = 0; //return code

	const char *fileContents;
	fileContents = loadTextFile(file);
	if (fileContents != 0) {
		//Compile shader
		GLuint shaderIndex = glCreateShader(shaderType);
		glShaderSource(shaderIndex, 1, &fileContents, 0);
		glCompileShader(shaderIndex);

		//Get shader status
		GLint compileStatus;
		glGetShaderiv(shaderIndex, GL_COMPILE_STATUS, &compileStatus);
		if (compileStatus == GL_FALSE) {
			std::cerr << "SHADER COMPILE ERROR!" << std::endl;
			GLint logLength;
			glGetShaderiv(shaderIndex, GL_INFO_LOG_LENGTH, &logLength);
			char *logInfo = new char[logLength];
			glGetShaderInfoLog(shaderIndex, logLength, 0, logInfo);
			std::cerr << logInfo << std::endl;
			glDeleteShader(shaderIndex); //Delete bad shader
			delete [] logInfo;
		}
		else {
			ret = shaderIndex;
		}
	}
	else {
		std::cerr << "Error opening " << file << std::endl;
	}

	delete [] fileContents;
	return ret;
}

GLuint loadShaderProgram(const char *vertexFile, const char *fragmentFile) {
	GLuint status = 0;
	GLuint fragmentShader = 0;
	GLuint vertexShader = loadShader(vertexFile, GL_VERTEX_SHADER);
	if (vertexShader != 0) {
		fragmentShader = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
	}

	if ((vertexShader != 0) && (fragmentShader != 0)) {
		GLuint program = glCreateProgram();
		glAttachShader(program, vertexShader);
		glAttachShader(program, fragmentShader);
		glLinkProgram(program);

		//Check to see if the thing linked correctly
		GLint linkStatus;
		glGetShaderiv(program, GL_LINK_STATUS, &linkStatus);
		if (linkStatus == GL_FALSE) {
			std::cerr << "PROGRAM LINK ERROR!" << std::endl;
			GLint logLength;
			glGetShaderiv(program, GL_INFO_LOG_LENGTH, &logLength);
			char *logInfo = new char[logLength];
			glGetShaderInfoLog(program, logLength, 0, logInfo);
			std::cerr << logInfo << std::endl;
			delete [] logInfo;
		}
		else {
			status = program;
		}
	}
	else {
		std::cerr << "Error loading shaders" << std::endl;
	}


	return status;
}

void setShaderColor(GLuint shaderProgramIndex, const char *variableName, float r, float g, float b) {
	GLint varLocation = glGetUniformLocation(shaderProgramIndex, variableName);
	if (varLocation == -1) {
		std::cerr << "Invalid var name:" << variableName << std::endl;
	}
	else {
		glProgramUniform4f(shaderProgramIndex, varLocation, r, g, b, 1.0f);
	}
}

void setShaderVec2(GLuint shaderProgramIndex, const char *variableName, const glm::vec2 &vecToSend) {
	GLint varLocation = glGetUniformLocation(shaderProgramIndex, variableName);
	if (varLocation == -1) {
		std::cerr << "Invalid var name: " << variableName << std::endl;
	}
	else {
		glProgramUniform2fv(shaderProgramIndex, varLocation, 1, &vecToSend[0]);
	}
}

void setShaderMatrix(GLuint shaderProgramIndex, const char *variableName, const glm::mat4 &matrixToSend) {
	GLint varLocation = glGetUniformLocation(shaderProgramIndex, variableName);
	if (varLocation == -1) {
		std::cerr << "Invalid var name: " << variableName << std::endl;
	}
	else {
		glProgramUniformMatrix4fv(shaderProgramIndex, varLocation, 1, GL_FALSE, &matrixToSend[0][0]);
	}
}