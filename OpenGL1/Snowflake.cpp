#include "Snowflake.h"

#include <random>

const float Snowflake::radius = 0.01f;

//Used to generate random positions
std::uniform_real_distribution<> Snowflake::startX(-1.0f, 1.0f);
std::uniform_real_distribution<> Snowflake::startY(1.1f, 2.0f);

float Snowflake::getRandomX() {
	return static_cast<float>(Snowflake::startX(gen));
}

float Snowflake::getRandomY() {
	return static_cast<float>(Snowflake::startY(gen));
}

Snowflake::Snowflake(Shape *s) :
	Entity(s, getRandomX(), getRandomY(), radius, radius, 0.2f)
{
}

Snowflake::~Snowflake() {
}

void Snowflake::update() {
	applyForce(glm::vec3(0.0f, -0.002f, 0.0f)); //Apply Gravity
	
	//Apply left/right friction so the snowflake stops blowing when wind does
	glm::vec3 friction = glm::vec3(-0.05f * m_velocity.x, 0.0f, 0.0f); 
	applyForce(friction);

	Entity::update();

	//Limit horizontal velocity
	if (m_velocity.x > 0.3f) {
		m_velocity.x = 0.3f;
	}
	else if (m_velocity.x < -0.3f) {
		m_velocity.x = -0.3f;
	}

	//Reset Y position if it goes off screen.
	if (m_position.y < -1.5f) {
		m_position = glm::vec3(getRandomX(), getRandomY(), 0.0f);
		m_velocity = glm::vec3(m_velocity.x, 0.0f, 0);
	}

	//Reset X position if it goes off screen.
	if (m_position.x < (-1.0f + (radius * 2))) {
		m_position.x = 1.0f + (radius * 2);
	}
	else if (m_position.x > (1.0f + (radius * 2))) {
		m_position.x = -1.0f + (radius * 2);
	}
}
