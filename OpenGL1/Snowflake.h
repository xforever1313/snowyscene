#pragma once

#include <random>

#include "Entity.h"

class Snowflake : public Entity {
	public:
		Snowflake(Shape *s);
		~Snowflake();

		void update() override;

	private:
		static float getRandomX();
		static float getRandomY();
		static std::uniform_real_distribution<> startX;
		static std::uniform_real_distribution<> startY;
		static const float radius;
};
